import tkinter as tk
from tkinter import Canvas
import numpy as np
from wyniki import dodaj_wynik, pobierz_wyniki, inicjalizuj_wyniki
from wyniki_dane import WYNIKI
WYNIKI_PLIK = 'wyniki_dane.py'
rozmiar_ekranu = 800
liczba_kropek = 8
kolor_kropek = '#1A96F6'
gracz1_kolor = '#F79B05'
gracz2_kolor = '#50EF6B'
wypelnienie_kwadratu1 = '#f5cd8c'
wypelnienie_kwadratu2 = '#9ff5ae'
zielony_kolor = '#7BC043'
szerokosc_kropki = 0.25 * rozmiar_ekranu / liczba_kropek
szerokosc_linia = 0.1 * rozmiar_ekranu / liczba_kropek
odleglosc_miedzy_kropkami = rozmiar_ekranu / (liczba_kropek)
pomaranczowy = '#F79B05'


# Klasa reprezentująca grę Dots and Boxes
class Kropki_i_kreski():

    def __init__(self, root):
        self.root = root
        self.root.title('-• Kropki i kreski •-')
        self.canvas = Canvas(self.root, width=rozmiar_ekranu, height=rozmiar_ekranu)
        self.canvas.pack()
        self.root.bind('<Button-1>', self.kliknij)
        self.zaczyna_gracz1 = True
        self.odswiez_tablice()
        self.status_tablicy = np.zeros(shape=(liczba_kropek - 1, liczba_kropek - 1))
        self.stan_wiersza = np.zeros(shape=(liczba_kropek, liczba_kropek - 1))
        self.stan_kolumny = np.zeros(shape=(liczba_kropek - 1, liczba_kropek))
        self.zaczyna_gracz1 = True
        self.player1_turn = not self.zaczyna_gracz1
        self.resetuj_tablice = False
        self.turntext_handle = []
        self.juz_pokolorowane_pudelko = []
        self.tura_gracza()
        inicjalizuj_wyniki()
        self.gra_wyniki = pobierz_wyniki()
        self.history_score_green = 0
        self.history_score_orange = 0
        self.game_over = False


    def bubble_sort(self, scores):

        sorted_scores = sorted(scores.items(), key=lambda x: x[1], reverse=True)
        return dict(sorted_scores)

    def zagraj_ponownie(self):
        # Resetting the board and setting initial parameters
        self.odswiez_tablice()
        self.status_tablicy = np.zeros(shape=(liczba_kropek - 1, liczba_kropek - 1))
        self.stan_wiersza = np.zeros(shape=(liczba_kropek, liczba_kropek - 1))
        self.stan_kolumny = np.zeros(shape=(liczba_kropek - 1, liczba_kropek))

        # Get player names again

        # Switch the starting player
        self.zaczyna_gracz1 = not self.zaczyna_gracz1
        self.player1_turn = not self.zaczyna_gracz1
        self.resetuj_tablice = False
        self.turntext_handle = []

        self.juz_pokolorowane_pudelko = []

        self.tura_gracza()

    def mainloop(self):
        # Rozpoczęcie głównej pętli gry
        self.root.mainloop()

    # logika gry:

    def czy_kratka_zajeta(self, pozycja, type):
        # Sprawdzenie, czy dana kratka planszy jest już zajęta
        r = pozycja[0]
        c = pozycja[1]
        zajeta = True

        if type == 'row' and self.stan_wiersza[c][r] == 0:
            zajeta = False
        if type == 'col' and self.stan_kolumny[c][r] == 0:
            zajeta = False

        return zajeta

    def konwertuj_siatke_do_pozycji(self, pozycja_siatki):
        # Konwersja pozycji kliknięcia myszą na pozycję logiczną na planszy
        pozycja_siatki = np.array(pozycja_siatki)
        position = (pozycja_siatki - odleglosc_miedzy_kropkami / 4) // (odleglosc_miedzy_kropkami / 2)

        type = False
        pozycja = []
        if position[1] % 2 == 0 and (position[0] - 1) % 2 == 0:
            r = int((position[0] - 1) // 2)
            c = int(position[1] // 2)
            pozycja = [r, c]
            type = 'row'
        elif position[0] % 2 == 0 and (position[1] - 1) % 2 == 0:
            c = int((position[1] - 1) // 2)
            r = int(position[0] // 2)
            pozycja = [r, c]
            type = 'col'

        return pozycja, type

    def pokoloruj_pudelka(self):
        # Zaznaczenie pudełka, jeśli zostało ukończone
        boxes = np.argwhere(self.status_tablicy == -4)
        for box in boxes:
            if list(box) not in self.juz_pokolorowane_pudelko and list(box) != []:
                self.juz_pokolorowane_pudelko.append(list(box))
                color = wypelnienie_kwadratu1 if self.player1_turn else wypelnienie_kwadratu2
                self.srodek_pudelka(box, color)

        boxes = np.argwhere(self.status_tablicy == 4)
        for box in boxes:
            if list(box) not in self.juz_pokolorowane_pudelko and list(box) != []:
                self.juz_pokolorowane_pudelko.append(list(box))
                color = wypelnienie_kwadratu2 if self.player1_turn else wypelnienie_kwadratu1
                self.srodek_pudelka(box, color)

    def zaktualizuj_tablice(self, type, pozycja):
        # Aktualizacja statusu planszy po ruchu gracza
        r = pozycja[0]
        c = pozycja[1]
        val = 1 if self.player1_turn else -1

        if c < (liczba_kropek - 1) and r < (liczba_kropek - 1):
            self.status_tablicy[c][r] += val

        if type == 'row':
            self.stan_wiersza[c][r] = 1
            if c >= 1:
                self.status_tablicy[c - 1][r] += val

        elif type == 'col':
            self.stan_kolumny[c][r] = 1
            if r >= 1:
                self.status_tablicy[c][r - 1] += val

    def czy_koniec_gry(self):
        if (self.stan_wiersza == 1).all() and (self.stan_kolumny == 1).all() and not np.any(self.resetuj_tablice):
            self.koniec_gry()
            return True
        return False

    # rysowanie na planszy:

    def make_edge(self, type, pozycja):
        # Rysowanie krawędzi planszy
        if type == 'row':
            start_x = odleglosc_miedzy_kropkami / 2 + pozycja[0] * odleglosc_miedzy_kropkami
            end_x = start_x + odleglosc_miedzy_kropkami
            start_y = odleglosc_miedzy_kropkami / 2 + pozycja[1] * odleglosc_miedzy_kropkami
            end_y = start_y
        elif type == 'col':
            start_y = odleglosc_miedzy_kropkami / 2 + pozycja[1] * odleglosc_miedzy_kropkami
            end_y = start_y + odleglosc_miedzy_kropkami
            start_x = odleglosc_miedzy_kropkami / 2 + pozycja[0] * odleglosc_miedzy_kropkami
            end_x = start_x

        color = gracz1_kolor if self.player1_turn else gracz2_kolor
        self.canvas.create_line(start_x, start_y, end_x, end_y, fill=color, width=szerokosc_linia)

    def koniec_gry(self):
        if not self.game_over:
            self.game_over = True

            player1_score = len(np.argwhere(self.status_tablicy == -4))
            player2_score = len(np.argwhere(self.status_tablicy == 4))

            self.player1_score = player1_score
            self.player2_score = player2_score

            if player1_score > player2_score:
                text = f'Wygrywa: Gracz 1 '
                color = '#7BC043'
                self.history_score_green += 1
            elif player2_score > player1_score:
                text = f'Wygrywa: Gracz 2 '
                color = pomaranczowy
                self.history_score_orange += 1
            else:
                text = 'Koniec gry!'
                color = 'gray'

            dodaj_wynik(player1_score, player2_score, WYNIKI)
            with open(WYNIKI_PLIK, 'w') as file:
                file.write("WYNIKI = " + repr(WYNIKI))


            self.canvas.delete("all")
            self.canvas.create_text(rozmiar_ekranu / 2, rozmiar_ekranu / 3, font="cmr 60 bold", fill=color, text=text)

            score_text = 'Wynik: \n'
            self.canvas.create_text(rozmiar_ekranu / 2, 5 * rozmiar_ekranu / 8, font="cmr 40 bold", fill=zielony_kolor,
                                    text=score_text)

            score_text = 'zielony ziomek : ' + str(player1_score) + '\n'
            score_text += 'pomaranczowy ziomek : ' + str(player2_score) + '\n'
            self.canvas.create_text(rozmiar_ekranu / 2, 3 * rozmiar_ekranu / 4, font="cmr 30 bold", fill=pomaranczowy,
                                    text=score_text)

            score_text = 'Zagraj jeszcze raz \n'
            play_again_button = self.canvas.create_text(rozmiar_ekranu / 2, 15 * rozmiar_ekranu / 16,
                                                        font="cmr 20 bold",
                                                        fill="gray", text=score_text)

            return_button = self.canvas.create_text(rozmiar_ekranu / 5, rozmiar_ekranu /15,
                                                    font="cmr 20 bold", fill="gray", text="Powrót do menu")

            self.canvas.tag_bind(play_again_button, '<Button-1>', lambda event: self.zagraj_ponownie())
            self.canvas.tag_bind(return_button, '<Button-1>', lambda event: self.menu_glowne())
            self.resetuj_tablice = True

            history_text = f'Historia wyników: \n Pomarańczowy: {self.history_score_orange} \n Zielony: {self.history_score_green} '
            self.canvas.create_text(rozmiar_ekranu / 2, 3 * rozmiar_ekranu / 14, font="cmr 30 bold", fill=pomaranczowy,
                                    text=history_text)

    def powrot_do_menu(self):
        self.canvas.delete("all")
        self.resetuj_tablice = True
        return_button = self.canvas.create_text(rozmiar_ekranu / 2, 15 * rozmiar_ekranu / 16,
                                                font="cmr 20 bold", fill="gray", text="Powrót do menu")
        self.canvas.tag_bind(return_button, '<Button-1>', lambda event: self.menu_glowne())

    def menu_glowne(self):
        self.resetuj_tablice = True
        self.canvas.delete("all")
        MainMenu(self.root).create_main_menu()

    def odswiez_tablice(self):
        for i in range(liczba_kropek):
            x = i * odleglosc_miedzy_kropkami + odleglosc_miedzy_kropkami / 2
            self.canvas.create_line(x, odleglosc_miedzy_kropkami / 2, x,
                                    rozmiar_ekranu - odleglosc_miedzy_kropkami / 2,
                                    fill='gray', dash=(2, 2))
            self.canvas.create_line(odleglosc_miedzy_kropkami / 2, x,
                                    rozmiar_ekranu - odleglosc_miedzy_kropkami / 2, x,
                                    fill='gray', dash=(2, 2))

        for i in range(liczba_kropek):
            for j in range(liczba_kropek):
                start_x = i * odleglosc_miedzy_kropkami + odleglosc_miedzy_kropkami / 2
                end_x = j * odleglosc_miedzy_kropkami + odleglosc_miedzy_kropkami / 2
                self.canvas.create_oval(start_x - szerokosc_kropki / 2, end_x - szerokosc_kropki / 2,
                                        start_x + szerokosc_kropki / 2,
                                        end_x + szerokosc_kropki / 2, fill=kolor_kropek,
                                        outline=kolor_kropek)

    def tura_gracza(self):
        text = 'Teraz kolej: '
        if self.player1_turn:
            text += f'Gracz 2'
            color = gracz1_kolor
        else:
            text += 'Gracz 1'
            color = gracz2_kolor

        self.canvas.delete(self.turntext_handle)
        self.turntext_handle = self.canvas.create_text(rozmiar_ekranu / 2,
                                                       rozmiar_ekranu - odleglosc_miedzy_kropkami / 8,
                                                       font="cmr 15 bold", text=text, fill=color)

    def srodek_pudelka(self, box, color):
        # Zaznaczenie pudełka kolorem
        start_x = odleglosc_miedzy_kropkami / 2 + box[1] * odleglosc_miedzy_kropkami + szerokosc_linia / 2
        start_y = odleglosc_miedzy_kropkami / 2 + box[0] * odleglosc_miedzy_kropkami + szerokosc_linia / 2
        end_x = start_x + odleglosc_miedzy_kropkami - szerokosc_linia
        end_y = start_y + odleglosc_miedzy_kropkami - szerokosc_linia
        self.canvas.create_rectangle(start_x, start_y, end_x, end_y, fill=color, outline='')

    def kliknij(self, event):
        if not self.resetuj_tablice and not self.game_over:
            pozycja_siatki = [event.x, event.y]
            pozycja, valid_input = self.konwertuj_siatke_do_pozycji(pozycja_siatki)
            if valid_input and not self.czy_kratka_zajeta(pozycja, valid_input):
                self.zaktualizuj_tablice(valid_input, pozycja)
                self.make_edge(valid_input, pozycja)
                self.pokoloruj_pudelka()
                self.odswiez_tablice()
                self.player1_turn = not self.player1_turn

                if self.czy_koniec_gry():
                    self.koniec_gry()
                else:
                    self.tura_gracza()
        elif self.game_over:
            self.canvas.delete("all")
            self.zagraj_ponownie()
            self.game_over = False
class MainMenu:
    def __init__(self, root):
        self.root = root
        self.root.title('-• Kropki i kreski •-')
        self.root.geometry('1920x1080')
        self.create_main_menu()
        self.root.configure(bg="light grey")

    def create_main_menu(self):
        self.clear_window()
        label = tk.Label(self.root, text="Kropki i kreski", bg='light grey', font=('Arial', 30, 'bold'))
        label.pack(padx=20, pady=200)

        buttonPlay = tk.Button(self.root, text="Graj", width=30, bg='lightblue', font=('Arial', 26),
                               command=self.start_game)
        buttonPlay.pack(pady=15)
        buttonHistory = tk.Button(self.root, text="Najwyższe wygrane", width=30, bg='lightblue', font=('Arial', 26),
                               command=self.pokaz_wyniki)
        buttonHistory.pack(pady=15)
        buttonExit = tk.Button(self.root, text="Wyjdź z gry", width=30, bg='lightblue', font=('Arial', 26),
                               command=self.root.destroy)
        buttonExit.pack(pady=15)

    def start_game(self):
        self.clear_window()
        game_instance = Kropki_i_kreski(self.root)
        game_instance.mainloop()

    def clear_window(self):
        for widget in self.root.winfo_children():
            widget.destroy()

    def pokaz_wyniki(self):
        self.clear_window()
        wyniki_label = tk.Label(self.root, text="Najwyższe wygrane", bg='light grey', font=('Arial', 30, 'bold'))
        wyniki_label.pack(padx=20, pady=50)

        # Pobierz wyniki z pliku wyniki_dane.py
        from wyniki_dane import WYNIKI
        if not WYNIKI:
            wyniki_text = "Brak dostępnych wyników."
        else:
            # Posortuj wyniki za pomocą bubble sort
            bubble_sort(WYNIKI)

            # Wybierz tylko 10 najwyższych wyników
            top_10 = WYNIKI[:10]

            wyniki_text = "Wyniki:\n\n"
            for idx, wynik in enumerate(top_10, start=1):
                wyniki_text += f"{idx}. Gracz 1: {wynik['Gracz1']}, Gracz 2: {wynik['Gracz2']}\n"

        wyniki_display = tk.Label(self.root, text=wyniki_text, font=('Arial', 20), bg='light grey')
        wyniki_display.pack(pady=50)

        return_button = tk.Button(self.root, text="Powrót do menu", width=30, bg='lightblue', font=('Arial', 26),
                                  command=self.create_main_menu)
        return_button.pack(pady=15)

def bubble_sort(scores):
    n = len(scores)
    for i in range(n - 1):
        for j in range(0, n - i - 1):
            if scores[j]['Gracz1'] + scores[j]['Gracz2'] < scores[j + 1]['Gracz1'] + scores[j + 1]['Gracz2']:
                scores[j], scores[j + 1] = scores[j + 1], scores[j]

if __name__ == "__main__":
    root = tk.Tk()
    app = MainMenu(root)
    root.mainloop()